﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterMovement : MonoBehaviour {
    public GameObject Cursor;

    private NavMeshAgent navMeshAgent;
    private GameObject destination;

    // Use this for initialization
    void Start () {
        navMeshAgent = GetComponent<NavMeshAgent> ();

        destination = Instantiate (Cursor, Vector3.zero, Quaternion.identity) as GameObject;
        destination.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown (1)) {
            RaycastHit hit;
            destination.SetActive (true);
            
            if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 100)) {
                Vector3 direction = new Vector3 (hit.point.x, transform.position.y, hit.point.z);
                destination.transform.position = direction;
                transform.LookAt(direction);

                navMeshAgent.SetDestination (direction);
            }

            if (transform.position == navMeshAgent.destination)
                destination.SetActive (false);
        }
    }
}