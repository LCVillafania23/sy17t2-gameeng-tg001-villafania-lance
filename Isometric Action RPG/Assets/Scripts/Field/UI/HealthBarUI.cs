﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUI : MonoBehaviour {
    public Image HealthBar;
    private Attributes characterAttributes;

    void Awake() {
        characterAttributes = GetComponentInParent<Attributes>();
    }

    void Update() {
        HealthBar.fillAmount = characterAttributes.GetHealthPercentage();
    }
}
