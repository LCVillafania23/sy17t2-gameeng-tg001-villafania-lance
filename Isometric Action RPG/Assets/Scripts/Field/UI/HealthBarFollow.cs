﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarFollow : MonoBehaviour {
    public enum CharacterName {
        Koko,
        Goble,
        Sla,
        WildPig
    }
    public CharacterName Name;
    private Transform parentTransform;
    private float offset;

    void Awake() {
        switch (Name) {
            case CharacterName.Koko:
                offset = 2.5f;
                break;
            case CharacterName.Goble:
                offset = 3f;
                break;
            case CharacterName.Sla:
                offset = 1.5f;
                break;
            case CharacterName.WildPig:
                offset = 2.25f;
                break;
        }
        parentTransform = transform.parent.parent;
    }

    void LateUpdate() {
        transform.position = new Vector3(parentTransform.position.x, parentTransform.position.y + offset, parentTransform.position.z);
        transform.rotation = Quaternion.identity;
    }
}
