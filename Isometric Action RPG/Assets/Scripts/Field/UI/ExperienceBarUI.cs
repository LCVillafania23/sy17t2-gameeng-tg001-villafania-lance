﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExperienceBarUI : MonoBehaviour {
    public Image ExperienceBar;
    private GameObject player;
    private PlayerAttributes playerAttributes;

    void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
        playerAttributes = player.gameObject.GetComponentInParent<PlayerAttributes>();
    }

    void Update() {
        ExperienceBar.fillAmount = playerAttributes.GetExperiencePercentage();
    }
}
