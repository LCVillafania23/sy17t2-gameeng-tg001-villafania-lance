﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attributes : MonoBehaviour {
    public int Vitality;
    public int Strength;
    public float Agility;
    protected int vitalityMultiplier;
    protected int strengthMultiplier;
    protected float agilityMultiplier;
    protected int maximumHealth;
    protected int currentHealth;
    protected int damage;
    protected float attackSpeed;
    protected float healthPercentage;

    protected virtual void Awake() {
        vitalityMultiplier = 100;
        strengthMultiplier = 10;
        agilityMultiplier = 1f;
    }
    
    protected virtual void SetDerivedAttributes() {
        maximumHealth = Vitality * vitalityMultiplier;
        currentHealth = maximumHealth;
        damage = Strength * strengthMultiplier;
        attackSpeed = Agility * agilityMultiplier;
    }

    protected virtual void SetHealthPercentage() {
        healthPercentage = (float)currentHealth / (float)maximumHealth;
    }

    public float GetHealthPercentage() {
        return healthPercentage;
    }
}
