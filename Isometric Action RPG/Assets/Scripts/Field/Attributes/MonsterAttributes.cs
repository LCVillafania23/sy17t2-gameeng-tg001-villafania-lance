﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAttributes : Attributes {
    public enum EnemyType {
        Goble,
        Sla,
        WildPig
    }
    public EnemyType Type;
    private GameObject player;
    private PlayerControl playerControl;
    private MonsterFSM monsterFSM;
    private int experiencePoints;
    private bool isDead;

    protected override void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
        playerControl = player.GetComponent<PlayerControl>();
        monsterFSM = GetComponent<MonsterFSM>();

        base.Awake();
        SetAttributes();
        base.SetDerivedAttributes();
    }

    void Update() {
        SetHealth();
        base.SetHealthPercentage();
    }

    void SetAttributes() {
        switch (Type) {
            case EnemyType.Goble:
                Vitality = 10;
                Strength = 6;
                Agility = 4;
                experiencePoints = 100;
                break;
            case EnemyType.Sla:
                Vitality = 8;
                Strength = 4;
                Agility = 2;
                experiencePoints = 70;
                break;
            case EnemyType.WildPig:
                Vitality = 12;
                Strength = 5;
                Agility = 3;
                experiencePoints = 85;
                break;
        }
    }

    void SetHealth() {
        if (currentHealth <= 0 && !isDead) {
            currentHealth = 0;
            isDead = true;
            playerControl.SetToIdle();
            monsterFSM.SetToDead();
        }
    }

    public int GetDamage() {
        return damage;
    }

    public float GetAttackSpeed() {
        return attackSpeed;
    }

    public bool IsDead()  {
        return isDead;
    }

    public int GainExperiencePoints() {
        return experiencePoints;
    }

    public void ReceiveDamage(int damage) {
        currentHealth -= damage;
    }
}
