﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttributes : Attributes {
    private GameObject previousTarget;
    private PlayerControl playerControl;
    private MonsterFSM monsterFSM;
    private MonsterAttributes monsterAttributes;
    private int maximumExperience;
    private int currentExperience;
    private float experiencePercentage;
    private bool experienceGained;
    private bool isDead;

    protected override void Awake() {
        playerControl = GetComponent<PlayerControl>();
        maximumExperience = 1000;
        currentExperience = 0;
        base.Awake();
        SetAttributes();
        base.SetDerivedAttributes();
    }

    void Update() {
        SelectTarget();
        SetHealth();
        base.SetHealthPercentage();
        SetExperience();
        SetExperiencePercentage();
    }
    
    void SetAttributes() {
        //Vitality = 20;
        //Strength = 10;
        //Agility = 5;
    }

    void SelectTarget() {
        if (playerControl.SelectedTarget != null) {
            monsterFSM = playerControl.SelectedTarget.gameObject.GetComponent<MonsterFSM>();
            monsterAttributes = playerControl.SelectedTarget.gameObject.GetComponent<MonsterAttributes>();

            if (playerControl.SelectedTarget != previousTarget)
                experienceGained = false; 

            previousTarget = playerControl.SelectedTarget.gameObject;
        }
    }

    void SetHealth() {
        if (currentHealth <= 0 && !isDead) {
            currentHealth = 0;
            isDead = true;
            playerControl.SetToDead();
            monsterFSM.SetToIdle();
        }
    }

    void SetExperience() {
        if (playerControl.SelectedTarget != null && monsterAttributes.IsDead() && !experienceGained) {
            currentExperience += monsterAttributes.GainExperiencePoints();
            experienceGained = true;
        } 
    }

    void SetExperiencePercentage() {
        experiencePercentage = (float)currentExperience / (float)maximumExperience;
    }

    public float GetExperiencePercentage() {
        return experiencePercentage;
    }

    public int GetDamage() {
        return damage;
    }

    public float GetAttackSpeed() {
        return attackSpeed;
    }

    public bool IsDead() {
        return isDead;
    }

    public void ReceiveDamage(int damage) {
        currentHealth -= damage;
    }
}
