﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAttackingRadius : MonoBehaviour {
    private GameObject player;
    private MonsterFSM monsterFSM;
    private MonsterDealDamage monsterAttack;
    private PlayerAttributes playerAttributes;

    void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
        monsterFSM = GetComponentInParent<MonsterFSM>();
        monsterAttack = GetComponentInParent<MonsterDealDamage>();
        playerAttributes = player.gameObject.GetComponent<PlayerAttributes>();
    }

    void Update()  {
        IsTargetDead();
    }

    void OnTriggerEnter(Collider coll) {
        if (coll.gameObject.tag == "Player")
            monsterAttack.CanAttack = true;
    }
    void OnTriggerStay(Collider coll) {
        if (coll.gameObject.tag == "Player")
            monsterAttack.CanAttack = true;
    }

    void OnTriggerExit(Collider coll) {
        if (coll.gameObject.tag == "Player") {
            monsterFSM.SetToChasing();
            monsterAttack.CanAttack = false;
        }
    }
    void IsTargetDead() {
        if (playerAttributes.IsDead())
            monsterAttack.CanAttack = false;
    }

    public void SetDamage(int damage) {
        playerAttributes.ReceiveDamage(damage);
    }
}
