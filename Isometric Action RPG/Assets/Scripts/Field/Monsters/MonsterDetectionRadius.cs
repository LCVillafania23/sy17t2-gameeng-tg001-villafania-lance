﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDetectionRadius : MonoBehaviour {
    private GameObject player;
    private MonsterFSM monsterFSM;
    
    void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
        monsterFSM = GetComponentInParent<MonsterFSM>();
    }

    void OnTriggerEnter(Collider coll) {
        if (coll.gameObject.tag == "Player")
            monsterFSM.SetToChasing();
    }

    void OnTriggerExit(Collider coll) {
        if (coll.gameObject.tag == "Player")
            monsterFSM.SetToPatrolling();
    }
}
