﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimation : MonoBehaviour {
    private MonsterFSM monsterFSM;
    private Animator anim;

    void Awake() {
        monsterFSM = GetComponent<MonsterFSM>();
        anim = GetComponent<Animator>();
    }

    void Update() {
        anim.SetInteger("State", monsterFSM.SetCurrentAnimation());
    }
}
