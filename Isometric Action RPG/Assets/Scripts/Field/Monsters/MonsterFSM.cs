﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterFSM : MonoBehaviour {
    public enum MonsterState {
        Idle,
        Patrolling,
        Chasing,
        Attacking,
        Standby,
        Dead
    }
    [HideInInspector]
    public MonsterState State;
    private GameObject player;
    private NavMeshAgent navAgent;
    private Transform target;
    private Vector3 initialPosition;
    private float patrolRadius;
    private float patrolTimer;
    private float currentTimer;
    private int currentAnimation;

    void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
        navAgent = GetComponent<NavMeshAgent>();
        initialPosition = transform.position;
        patrolRadius = 5f;
        patrolTimer = 5f;
        currentTimer = patrolTimer;
    }

    void Update() {
        CheckState();
    }

    void CheckState() {
        float neutralSpeed = 1.5f;
        float hostileSpeed = 2.5f;

        switch (State) {
            case MonsterState.Idle:
                navAgent.speed = neutralSpeed;
                currentAnimation = 0;
                PatrolMonster();
                break;
            case MonsterState.Patrolling:
                navAgent.speed = neutralSpeed;
                currentAnimation = 1;
                PatrolMonster();
                break;
            case MonsterState.Chasing:
                navAgent.speed = hostileSpeed;
                currentAnimation = 2;
                MoveToPlayer();
                break;
            case MonsterState.Attacking:
                navAgent.speed = 0f;
                navAgent.velocity = Vector3.zero;
                currentAnimation = 3;
                MoveToPlayer();
                break;
            case MonsterState.Standby:
                navAgent.speed = 0f;
                navAgent.velocity = Vector3.zero;
                currentAnimation = 0;
                break;
            case MonsterState.Dead:
                navAgent.speed = 0f;
                navAgent.velocity = Vector3.zero;
                currentAnimation = 4;
                StartCoroutine(DestroyObject());
                break;
        }
    }

    void PatrolMonster() {
        currentTimer += Time.deltaTime;

        if (currentTimer >= patrolTimer && !HostileState()) {
            Vector3 randomPosition = Random.insideUnitSphere * patrolRadius;
            randomPosition += initialPosition;
            transform.LookAt(randomPosition);
            navAgent.SetDestination(randomPosition);
            currentTimer = 0f;
        }
        SetToPatrolling();
        CheckArrival();
    }

    void CheckArrival() {
        if (transform.position == navAgent.destination && !HostileState())
            SetToIdle();
    }

    IEnumerator DestroyObject() {
        float destroyDelay = 2.5f;
        yield return new WaitForSeconds(destroyDelay);
        Destroy(gameObject);
    }


    void MoveToPlayer() {
        Vector3 playerPosition = player.transform.position;
        transform.LookAt(playerPosition);
        navAgent.SetDestination(playerPosition);
    }

    bool HostileState() {
        if (State == MonsterState.Chasing || State == MonsterState.Attacking)
            return true;
        else
            return false;
    }

    public void SetToIdle() {
        State = MonsterState.Idle;
    }

    public void SetToPatrolling() {
        State = MonsterState.Patrolling;
    }

    public void SetToChasing() {
        State = MonsterState.Chasing;
    }

    public void SetToAttacking() {
        State = MonsterState.Attacking;
    }
    public void SetToStandby() {
        State = MonsterState.Standby;
    }
    public void SetToDead() {
        State = MonsterState.Dead;
    }

    public int SetCurrentAnimation() {
        return currentAnimation;
    }
}
