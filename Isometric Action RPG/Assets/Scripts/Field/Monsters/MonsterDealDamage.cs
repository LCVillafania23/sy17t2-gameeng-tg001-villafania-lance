﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDealDamage : MonoBehaviour {
    [HideInInspector]
    public float AttackSpeed;
    [HideInInspector]
    public bool CanAttack;
    private MonsterFSM monsterFSM;
    private MonsterAttributes monsterAttribute;
    private MonsterAttackingRadius monsterAttack;
    private bool hasAttacked;

    void Awake() {
        monsterFSM = GetComponent<MonsterFSM>();
        monsterAttribute = GetComponent<MonsterAttributes>();
        monsterAttack = GetComponentInChildren<MonsterAttackingRadius>();
    }

    void Update() {
        if (CanAttack && !hasAttacked)
            StartCoroutine(DealDamage());
        else if (!CanAttack) {
            StopAllCoroutines();
            hasAttacked = false;
        }
    }

    IEnumerator DealDamage() {
        float animationDelay = 0.5f;

        monsterFSM.SetToAttacking();
        hasAttacked = true;
        yield return new WaitForSeconds(animationDelay);
        monsterAttack.SetDamage(monsterAttribute.GetDamage());
        StartCoroutine(AnimationDelay());
    }

    IEnumerator AnimationDelay() {
        float maximumDelay = 1.5f;
        float attackSpeedMultiplier = 0.1f;
        float damageDelay = maximumDelay - (monsterAttribute.GetAttackSpeed() * attackSpeedMultiplier);

        monsterFSM.SetToStandby();
        yield return new WaitForSeconds(damageDelay);
        StartCoroutine(DamageDelay());
    }

    IEnumerator DamageDelay() {
        monsterFSM.SetToAttacking();
        hasAttacked = false;
        yield return null;
    }
}
