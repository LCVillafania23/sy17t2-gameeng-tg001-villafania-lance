﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour {
    public enum EnemyType : int {
        Goble = 0,  
        Sla = 1,
        WildPig = 2
    }
    public List<GameObject> Enemy;
    public List<GameObject> Waypoint;
    private GameObject enemyInstance;
    private List<GameObject> waypointInstance = new List<GameObject>();
    private int randomEnemy;
    private int randomWaypoint;
    private int waypointSpawned;
    private float initialSpawnDelay = 3f;
    private float spawnInterval = 10f;

    void Start() {
        waypointSpawned = 0;
        InstantiateWaypoints();
        StartCoroutine(SpawnEnemies());
    }

    void InstantiateWaypoints() {
        for (int i = 0; i < Waypoint.Count; i++) {
            waypointInstance.Add(Instantiate(Waypoint[i], Waypoint[i].transform.position, Quaternion.identity) as GameObject);
        }
    }

    IEnumerator SpawnEnemies() {
        yield return new WaitForSeconds(initialSpawnDelay);

        for (int i = 0; i < Waypoint.Count; i++) {
            SpawnAtRandom();
            RemoveSpawnedWaypoint();
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    void SpawnAtRandom() {
        randomEnemy = Random.Range((int)EnemyType.Goble, (int)EnemyType.WildPig + 1);
        randomWaypoint = Random.Range(0, Waypoint.Count - 1 - waypointSpawned);
        enemyInstance = Instantiate(Enemy[randomEnemy], waypointInstance[randomWaypoint].transform.position, Quaternion.identity) as GameObject;
    }

    void RemoveSpawnedWaypoint() {
        waypointInstance.Remove(waypointInstance[randomWaypoint]);
        waypointSpawned++;
    }
}
