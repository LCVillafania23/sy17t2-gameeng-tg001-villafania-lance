﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDealDamage : MonoBehaviour {
    [HideInInspector]
    public float AttackSpeed;
    [HideInInspector]
    public bool CanAttack;
    private PlayerControl playerControl;
    private PlayerAttributes playerAttribute;
    private PlayerAttackingRadius playerAttack;
    private bool hasAttacked;

    void Awake() {
        playerControl = GetComponent<PlayerControl>();
        playerAttribute = GetComponent<PlayerAttributes>();
        playerAttack = GetComponentInChildren<PlayerAttackingRadius>();
    }

    void Update() {
        if (CanAttack && !hasAttacked)
            StartCoroutine(DealDamage());
        else if (!CanAttack) {
            StopAllCoroutines();
            hasAttacked = false;
        }
    }

    IEnumerator DealDamage() {
        float animationDelay = 0.5f;

        playerControl.SetToAttacking();
        hasAttacked = true;
        yield return new WaitForSeconds(animationDelay);
        playerAttack.SetDamage(playerAttribute.GetDamage());
        StartCoroutine(AnimationDelay());
    }

    IEnumerator AnimationDelay() {
        float maximumDelay = 1.5f;
        float attackSpeedMultiplier = 0.1f;
        float damageDelay = maximumDelay - (playerAttribute.GetAttackSpeed() * attackSpeedMultiplier);

        playerControl.SetToStandby();
        yield return new WaitForSeconds(damageDelay);
        StartCoroutine(DamageDelay());
    }

    IEnumerator DamageDelay() {
        playerControl.SetToAttacking();
        hasAttacked = false;
        yield return null;
    }
}
