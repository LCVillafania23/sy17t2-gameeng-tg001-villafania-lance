﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {
    private PlayerControl playerControl;
    private Animator anim;

    void Awake() {
        playerControl = GetComponent<PlayerControl>();
        anim = GetComponent<Animator>();
    }

    void Update() {
        anim.SetInteger("State", playerControl.SetCurrentAnimation());
    }
}
