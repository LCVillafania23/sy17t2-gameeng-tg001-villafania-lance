﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackingRadius : MonoBehaviour {
    private enum PlayerState {
        Running,
        Chasing,
        Attacking
    }
    private PlayerState state;
    private PlayerControl playerControl;
    private PlayerDealDamage playerAttack;
    private MonsterAttributes monsterAttributes;

    void Awake() {
        playerControl = GetComponentInParent<PlayerControl>();
        playerAttack = GetComponentInParent<PlayerDealDamage>();
    }

    void Update() {
        CheckState();
        SelectTarget();
    }

    void OnTriggerEnter(Collider coll) {
        if (coll.gameObject.tag == "Enemy" && state == PlayerState.Chasing)
            playerAttack.CanAttack = true;
    }

    void OnTriggerStay(Collider coll) {
        if (coll.gameObject.tag == "Enemy" && state == PlayerState.Running)
            playerAttack.CanAttack = false;
        else if (coll.gameObject.tag == "Enemy" && state == PlayerState.Chasing|| coll.gameObject.tag == "Enemy" && state == PlayerState.Attacking)
            playerAttack.CanAttack = true;
    }

    void OnTriggerExit(Collider coll) {
        if (coll.gameObject.tag == "Enemy" && state == PlayerState.Running || coll.gameObject.tag == "Enemy" && state == PlayerState.Chasing)
            playerAttack.CanAttack = false;
    }

    void CheckState() {
        if (playerControl.GetState() == PlayerControl.PlayerState.Running)
            state = PlayerState.Running;
        else if (playerControl.GetState() == PlayerControl.PlayerState.Chasing)
            state = PlayerState.Chasing;
        else if (playerControl.GetState() == PlayerControl.PlayerState.Attacking)
            state = PlayerState.Attacking;
    }

    void SelectTarget() {
        if (playerControl.SelectedTarget != null) {
            monsterAttributes = playerControl.SelectedTarget.gameObject.GetComponent<MonsterAttributes>();
            IsTargetDead();
        }
    }

    void IsTargetDead() {
        if (monsterAttributes.IsDead())
            playerAttack.CanAttack = false;
    }

    public void SetDamage(int damage) {
        monsterAttributes.ReceiveDamage(damage);
    }
}
