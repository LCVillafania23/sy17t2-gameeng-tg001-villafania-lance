﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
    public Transform LookAt;
	private Vector3 offset;
	private Vector3 velocity = Vector3.zero;
	private float delay = 0.5f;
    
	void Start() {
        offset = new Vector3(0.0f, 15.0f, 15.0f);
    }
	
	void LateUpdate() {
		Vector3 desiredPosition = LookAt.transform.position + offset;
		transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, delay);
	}
}
