﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerControl : MonoBehaviour {
    public enum PlayerState {
        Idle,
        Running,
        Chasing,
        Attacking,
        Standby,
        Dead
    }
    [HideInInspector]
    public PlayerState State;
    [HideInInspector]
    public GameObject Cursor;
    [HideInInspector]
    public GameObject SelectedTarget;
    [HideInInspector]
    public bool HasInput;
    private GameObject destination;
    private NavMeshAgent navAgent;
    private int currentAnimation;
    
    void Awake() {
        destination = Instantiate(Cursor, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
        destination.SetActive(false);
        navAgent = GetComponent<NavMeshAgent>();
        SelectedTarget = null;
    }
	
	void Update() {
        CheckState();
        CheckInput();
    }

    void CheckState() {
        switch (State) {
            case PlayerState.Idle:
                destination.SetActive(false);
                navAgent.velocity = Vector3.zero;
                currentAnimation = 0;
                break;
            case PlayerState.Running:
                destination.SetActive(true);
                currentAnimation = 1;
                break;
            case PlayerState.Chasing:
                destination.SetActive(false);
                currentAnimation = 2;
                break;
            case PlayerState.Attacking:
                destination.SetActive(false);
                navAgent.velocity = Vector3.zero;
                currentAnimation = 3;
                break;
            case PlayerState.Standby:
                destination.SetActive(false);
                navAgent.velocity = Vector3.zero;
                currentAnimation = 4;
                break;
            case PlayerState.Dead:
                destination.SetActive(false);
                navAgent.velocity = Vector3.zero;
                currentAnimation = 5;
                StartCoroutine(DestroyObject());
                break;
        }
    }

    void CheckInput() {
        Vector3 mouseInput = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(mouseInput);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100f) && HasInput) {
            Vector3 destinationPosition;

            if (hit.collider.tag == "Untagged") {
                SelectedTarget = null;
                destinationPosition = new Vector3(hit.point.x, hit.point.y + 0.2f, hit.point.z);
                MovePlayer(destinationPosition);
                SetToRunning();
            }
            if (hit.collider.tag == "Enemy") {
                SelectedTarget = hit.collider.gameObject;
                destinationPosition = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                MovePlayer(destinationPosition);
                SetToChasing();
            }
        }
        CheckArrival();
    }

    void MovePlayer(Vector3 destinationPosition) {
        destination.transform.position = destinationPosition;
        transform.LookAt(destination.transform.position);
        navAgent.SetDestination(destination.transform.position);
    }

    void CheckArrival() {
        if (transform.position == navAgent.destination && State == PlayerState.Running)
            SetToIdle();
    }

    IEnumerator DestroyObject() {
        float destroyDelay = 4f;
        yield return new WaitForSeconds(destroyDelay);
        Destroy(gameObject);
    }

    public void SetToIdle() {
        State = PlayerState.Idle;
    }

    public void SetToRunning() {
        State = PlayerState.Running;
    }

    public void SetToChasing() {
        State = PlayerState.Chasing;
    }

    public void SetToAttacking() {
        State = PlayerState.Attacking;
    }

    public void SetToStandby() {
        State = PlayerState.Standby;
    }
    public void SetToDead() {
        State = PlayerState.Dead;
    }

    public PlayerState GetState() {
        return State;
    }

    public int SetCurrentAnimation() {
        return currentAnimation;
    }
}