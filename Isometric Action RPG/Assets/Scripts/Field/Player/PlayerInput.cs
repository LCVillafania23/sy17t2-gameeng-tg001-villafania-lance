﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
    private PlayerControl playerControl;

    void Start() {
        playerControl = GetComponent<PlayerControl>();
    }
	
	void Update() {
        SetControl();
    }

    void SetControl() {
        if (Input.GetButtonDown("MovePlayer"))
            playerControl.HasInput = true;
        else if (Input.GetButtonDown("HaltPlayer"))
            playerControl.SetToIdle();
        else
            playerControl.HasInput = false;
    }
}
