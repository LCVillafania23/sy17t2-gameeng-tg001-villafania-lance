﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
    public Transform LookAt;

	private Vector3 offset;
	private Vector3 velocity = Vector3.zero;
	private float delay = 0.5f;

	// Use this for initialization
	void Start () {
		offset = new Vector3 (LookAt.transform.position.x + 20.0f, LookAt.transform.position.y + 18.0f, LookAt.transform.position.z + 9.0f);
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 desiredPosition = LookAt.transform.position + offset;
		transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, delay);
	}
}
