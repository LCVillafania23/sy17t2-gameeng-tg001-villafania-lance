﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Animations : MonoBehaviour {
	public List<GameObject> Characters = new List<GameObject> ();
	public List<Button> CharacterButtons = new List<Button> ();
	public List<Button> PlayerAnimationButtons = new List<Button> ();
	public List<Button> MonsterAnimationButtons = new List<Button> ();
	public List<Button> NPCAnimationButtons = new List<Button> ();

	private bool displayPlayer;
	private bool displayMonster;
	private bool displayNPC;

    // Use this for initialization
    void Start () {
		for (int i = 0; i < CharacterButtons.Count; i++) {
			int buttonSelection = i;
			CharacterButtons [i].onClick.AddListener (() => ShowAnimationList (buttonSelection));
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ShowAnimationList (int characterType) {
		switch (characterType) {
		case 0:
			if (!displayPlayer && !displayMonster && !displayNPC) {
				Characters [0].gameObject.SetActive (true);

				for (int i = 0; i < PlayerAnimationButtons.Count; i++) {
					int buttonSelection = i;
					PlayerAnimationButtons [i].gameObject.SetActive (true);
					PlayerAnimationButtons [i].onClick.AddListener (() => PlayerAnimationList (buttonSelection));
				} 
				displayPlayer = true;
			}
			else if (displayPlayer) {
				Characters [0].gameObject.SetActive (false);

				for (int i = 0; i < PlayerAnimationButtons.Count; i++) {
					PlayerAnimationButtons [i].gameObject.SetActive (false);
				}
				displayPlayer = false;
			}
			break;

		case 1:
			if (!displayPlayer && !displayMonster && !displayNPC) {
				Characters [1].gameObject.SetActive (true);

				for (int i = 0; i < MonsterAnimationButtons.Count; i++) {

					int buttonSelection = i;
					MonsterAnimationButtons [i].gameObject.SetActive (true);
					MonsterAnimationButtons [i].onClick.AddListener (() => MonsterAnimationList (buttonSelection));
				} 
				displayMonster = true;
			}
			else if (displayMonster) {
				Characters [1].gameObject.SetActive (false);

				for (int i = 0; i < MonsterAnimationButtons.Count; i++) {
					MonsterAnimationButtons [i].gameObject.SetActive (false);
				}
				displayMonster = false;
			}
			break;

		case 2:
			if (!displayPlayer && !displayMonster && !displayNPC) {
				Characters [2].gameObject.SetActive (true);

				for (int i = 0; i < NPCAnimationButtons.Count; i++) {
					int buttonSelection = i;
					NPCAnimationButtons [i].gameObject.SetActive (true);
					NPCAnimationButtons [i].onClick.AddListener (() => NPCAnimationList (buttonSelection));
				} 
				displayNPC = true;
			}
			else if (displayNPC) {
				Characters [2].gameObject.SetActive (false);

				for (int i = 0; i < NPCAnimationButtons.Count; i++) {
					NPCAnimationButtons [i].gameObject.SetActive (false);
				}
				displayNPC = false;
			}
			break;
		}
	}

	void PlayerAnimationList (int i) {
		switch (i) {
		case 0:
			Characters [0].GetComponent<Animator> ().Play ("KK_Idle");
			break;
		case 1:
			Characters [0].GetComponent<Animator> ().Play ("KK_Run_No");
			break;
		case 2:
			Characters [0].GetComponent<Animator> ().Play ("KK_Run");
			break;
		case 3:
			Characters [0].GetComponent<Animator> ().Play ("KK_Attack");
			break;
		case 4:
			Characters [0].GetComponent<Animator> ().Play ("KK_Attack_Standy");
			break;
		case 5:
			Characters [0].GetComponent<Animator> ().Play ("KK_Combo");
			break;
		case 6:
			Characters [0].GetComponent<Animator> ().Play ("KK_DrawBlade");
			break;
		case 7:
			Characters [0].GetComponent<Animator> ().Play ("KK_PutBlade");
			break;
		case 8:
			Characters [0].GetComponent<Animator> ().Play ("KK_Damage");
			break;
		case 9:
			Characters [0].GetComponent<Animator> ().Play ("KK_Skill");
			break;
		case 10:
			Characters [0].GetComponent<Animator> ().Play ("KK_Dead");
			break;
		}
	}

	void MonsterAnimationList (int i) {
		switch (i) {
		case 0:
			break;
		}
	}

	void NPCAnimationList (int i) {
		switch (i) {
		case 0:
			break;
		}
	}
}